package com.example.android.mdpandroid;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class MessagingFragment extends Fragment {
    TextView msgTxt;
    Button sendStuff;
    ScrollView msgScrollView;
    EditText editText;
    StringBuilder stringBuilder;
    public MessagingFragment() {
        // Required empty public constructor
        Log.d("debug" , "create mesage frage");
        stringBuilder = new StringBuilder();

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViewByIdes(view);
        implementListeners();
        msgTxt.setText(stringBuilder);
        msgScrollView.fullScroll(View.FOCUS_DOWN);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_messaging, container, false);
    }
    private void findViewByIdes(View view)
    {
        msgScrollView = view.findViewById(R.id.msgScrollView);
        msgTxt = view.findViewById(R.id.msgTextBox);
        sendStuff = view.findViewById(R.id.sendStuff);
        editText = view.findViewById(R.id.plain_text_input);
    }
    private void sendText(){
        String string= String.valueOf(editText.getText());
        Log.d("debug", "send!" + string);
        BluetoothConnectionService.write(string.getBytes());
    }
    private void implementListeners(){
        sendStuff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendText();
            }
        });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState!=null)
        {
            stringBuilder = new StringBuilder(savedInstanceState.getString("msgs" , ""));
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("msgs",stringBuilder.toString());
    }

    public void setTextBox(String msg)
    {
        stringBuilder.append(msg + "\n");
        msgTxt.setText(stringBuilder);
        msgScrollView.fullScroll(View.FOCUS_DOWN);
    }
}
