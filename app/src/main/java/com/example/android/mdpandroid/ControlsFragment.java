package com.example.android.mdpandroid;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.Toast;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ControlsFragment extends Fragment implements SensorEventListener {
    CommandPref commands;
    Button functionOneBtn , functionTwoBtn , beginFastestPathBtn , beginExploreBtn;
    ImageButton downBtn , leftBtn , rightBtn , upBtn ,voiceButton,lockBtn,rotateLeftBtn ,rotateRightBtn ;
    Switch tiltSwitch;
    boolean hasTilted , lockedControls;
    private SensorManager sensorManager;
    private Sensor sensor;
    private SpeechRecognizer speechRecognizer;
    private Intent intentRecognizer;
    public ControlsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        commands = CommandPref.getInstance(getActivity());
        hasTilted = false;
        lockedControls = false;
        findViewByIdes(view);

        sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.RECORD_AUDIO}, PackageManager.PERMISSION_GRANTED);
        intentRecognizer = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intentRecognizer.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(getContext());
        implementListeners();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_controls, container, false);
    }
    private void findViewByIdes(View view)
    {
        lockBtn = view.findViewById(R.id.lock_btn);
        functionOneBtn = view.findViewById(R.id.function_1_btn);
        functionTwoBtn = view.findViewById(R.id.function_2_btn);
//        rotateLeftBtn = view.findViewById(R.id.rotate_left_btn);
//        rotateRightBtn = view.findViewById(R.id.rotate_right_btn);
        downBtn = view.findViewById(R.id.down_btn);
        leftBtn = view.findViewById(R.id.left_btn);
        upBtn = view.findViewById(R.id.up_btn);
        rightBtn = view.findViewById(R.id.right_btn);
        tiltSwitch = view.findViewById(R.id.tilt_switch);
        beginFastestPathBtn = view.findViewById(R.id.begin_fastest_path_btn);
        beginExploreBtn = view.findViewById(R.id.begin_exploration_btn);
        voiceButton = view.findViewById(R.id.voice_btn);
    }
    private void implementListeners(){
        lockBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lockedControls = !lockedControls;
                if(lockedControls == true)
                {
                    lockBtn.setColorFilter(Color.parseColor("#d9534f"));
                }else
                {
                    lockBtn.setColorFilter(Color.parseColor("#F7F7F7"));
                }
            }
        });
        beginExploreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendCommand(commands.getBeginExplore());
                lockedControls = true;
                lockBtn.setColorFilter(Color.parseColor("#d9534f"));
            }
        });
        beginFastestPathBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendCommandWithNoLock(commands.getBeginFastPath());
                lockedControls = true;
                lockBtn.setColorFilter(Color.parseColor("#d9534f"));
            }
        });
        upBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendCommand(commands.getForward());
            }
        });
        downBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendCommand(commands.getReverse());
            }
        });
        leftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendCommand(commands.getRotateLeft());
            }
        });
        rightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendCommand(commands.getRotateRight());
            }
        });
//        rotateLeftBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                sendCommand(commands.getRotateLeft());
//            }
//        });
//        rotateRightBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                sendCommand(commands.getRotateRight());
//            }
//        });
        functionOneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendCommand(commands.getFunctionOne());
            }
        });
        functionTwoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendCommand(commands.getFunctionTwo());
            }
        });
        tiltSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                registerTiltMotion(isChecked);
            }
        });
        voiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(lockedControls == true)
                {
                    Toast.makeText(getContext(),"Controls are locked" , Toast.LENGTH_SHORT).show();
                }
                else
                {
                    speechRecognizer.startListening(intentRecognizer);
                    voiceButton.setColorFilter(Color.parseColor("#5cb85c"));
                    Toast.makeText(getContext(),"Listening" , Toast.LENGTH_LONG).show();
                }
            }
        });
        speechRecognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle params) {

            }

            @Override
            public void onBeginningOfSpeech() {

            }

            @Override
            public void onRmsChanged(float rmsdB) {

            }

            @Override
            public void onBufferReceived(byte[] buffer) {

            }

            @Override
            public void onEndOfSpeech() {
                voiceButton.setColorFilter(Color.parseColor("#F7F7F7"));
            }

            @Override
            public void onError(int error) {
                voiceButton.setColorFilter(Color.parseColor("#F7F7F7"));
            }

            @Override
            public void onResults(Bundle results) {
                ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                String string = "";
                if(matches!=null){
                    string = matches.get(0);
                    sendCommand(string);
                    Log.d("debug" , "sent string");
                    Toast.makeText(getContext(),string , Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onPartialResults(Bundle partialResults) {

            }

            @Override
            public void onEvent(int eventType, Bundle params) {

            }
        });
    }
    private void sendCommand(String command){
        if(lockedControls == true)
        {
            Toast.makeText(getContext(),"Controls are locked" , Toast.LENGTH_LONG).show();
        }
        else
        {
            Log.d("debug" , "sent command");
            BluetoothConnectionService.write(command.getBytes());
        }
    }
    private void sendCommandWithNoLock(String command){
            Log.d("debug" , "sent command");
            BluetoothConnectionService.write(command.getBytes());
    }
    private void registerTiltMotion(boolean enableMotion)
    {
        if(enableMotion)
        {
            //REGISTER TILT MOTION SENSOR
            sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
        else
        {
            sensorManager.unregisterListener(this,sensor);
        }
    }
    @Override
    public void onSensorChanged(SensorEvent event) {
        float x = event.values[0];
        float y = event.values[1];
        if(!hasTilted)
        {
            if (Math.abs(x) > Math.abs(y)) { //horizontal tilt
                if (x > 3) {
                    sendCommand(commands.getStrafeLeft());
                    hasTilted = true;
                }
                else if(x < -3)
                {
                    sendCommand(commands.getStrafeRight());
                    hasTilted = true;
                }
            }
            else
            {
                if (y > 3) {
                    sendCommand(commands.getReverse());
                    hasTilted = true;
                }
                else if(y < -3)
                {
                    sendCommand(commands.getForward());
                    hasTilted = true;
                }
            }
        }
        else if (Math.abs(x) < 3 && Math.abs(y) <3)
        {
            hasTilted = false;
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
