package com.example.android.mdpandroid;

import android.app.IntentService;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.UUID;


public class BluetoothConnectionService extends IntentService {

    private static final String TAG = "BTConnectionAService";
    private static final String appName = "Mdp_Group22_2020";

    //UUID
    private static final UUID mdpUUID = UUID.fromString ("00001101-0000-1000-8000-00805F9B34FB");
    public static final String LAST_MAC = "LAST_MAC";

    private BluetoothAdapter myBluetoothAdapter;

    private AcceptThread myAcceptThread;
    private ConnectThread myConnectThread;
    public  BluetoothDevice myDevice;
    private UUID deviceUUID;
    private static ConnectedThread mConnectedThread;

    Context myContext;

    //CONSTRUCTOR
    public BluetoothConnectionService() {

        super("BluetoothConnectionService");
    }

    //HANDLE INTENT FOR SERVICE. START THIS METHOD WHEN THE SERVICE IS CREATED
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        myContext = getApplicationContext();
        myBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        Log.d(TAG, "handleintent");
        if (intent.getStringExtra("serviceType").equals("listen")) {

            myDevice = (BluetoothDevice) intent.getExtras().getParcelable("device");

            Log.d(TAG, "Service Handle: startAcceptThread");

            startAcceptThread();
        } else {
            myDevice = (BluetoothDevice) intent.getExtras().getParcelable("device");
            deviceUUID = (UUID) intent.getSerializableExtra("id");

            Log.d(TAG, "Service Handle: startClientThread");

            startClientThread(myDevice, deviceUUID);
        }

    }

    /*
         A THREAD THAT RUNS WHILE LISTENING FOR INCOMING CONNECTIONS. IT BEHAVES LIKE
         A SERVER-SIDE CLIENT. IT RUNS UNTIL A CONNECTION IS ACCEPTED / CANCELLED
    */
    private class AcceptThread extends Thread {

        //Local server socket
        private final BluetoothServerSocket myServerSocket;

        public AcceptThread() {
            BluetoothServerSocket temp = null;

            //Create a new listening server socket
            try {
                temp = myBluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord(appName, mdpUUID);
                Log.d(TAG, "AcceptThread: Setting up server using: " + mdpUUID);

            } catch (IOException e) {

            }

            myServerSocket = temp;
        }

        public void run() {

            Log.d(TAG, "AcceptThread: Running");

            BluetoothSocket socket = null;
            Intent connectionStatusIntent;

            try {

                Log.d(TAG, "Run: RFCOM server socket start....");

                //Blocking call which will only return on a successful connection / exception
                socket = myServerSocket.accept();

                //BROADCAST CONNECTION MSG
                connectionStatusIntent = new Intent("btConnectionStatus");
                connectionStatusIntent.putExtra("ConnectionStatus", "connect");
                connectionStatusIntent.putExtra("Device", socket.getRemoteDevice());
                LocalBroadcastManager.getInstance(myContext).sendBroadcast(connectionStatusIntent);

                //Successfully connected
                Log.d(TAG, "Run: RFCOM server socket accepted connection");

                connected(socket,myDevice);

            } catch (IOException e) {

                connectionStatusIntent = new Intent("btConnectionStatus");
                connectionStatusIntent.putExtra("ConnectionStatus", "connectionFail");
                connectionStatusIntent.putExtra("Device",  BluetoothActivity.getBluetoothDevice());

                Log.d(TAG, "AcceptThread: Connection Failed ,IOException: " + e.getMessage());
            }


            Log.d(TAG, "Ended AcceptThread");

        }

        public void cancel() {

            Log.d(TAG, "Cancel: Canceling AcceptThread");

            try {
                myServerSocket.close();
            } catch (IOException e) {
                Log.d(TAG, "Cancel: Closing AcceptThread Failed. " + e.getMessage());
            }
        }


    }

    /*
        THREAD RUNS WHILE ATTEMPTING TO MAKE AN OUTGOING CONNECTION WITH A DEVICE.
        IT RUNS STRAIGHT THROUGH, MAKING EITHER A SUCCESSFULLY OR FAILED CONNECTION
    */
    private class ConnectThread extends Thread {

        private BluetoothSocket mySocket;

        public ConnectThread(BluetoothDevice device, UUID uuid) {

            Log.d(TAG, "ConnectThread: started");
            myDevice = device;
            deviceUUID = uuid;
        }

        public void run() {
            BluetoothSocket temp = null;
            Intent connectionStatusIntent;

            Log.d(TAG, "Run: myConnectThread");

            /*
            Get a BluetoothSocket for a
            connection with given BluetoothDevice
            */
            try {
                Log.d(TAG, "ConnectThread: Trying to create InsecureRFcommSocket using UUID: " + mdpUUID);
                temp = myDevice.createRfcommSocketToServiceRecord(deviceUUID);
            } catch (IOException e) {

                Log.d(TAG, "ConnectThread: Could not create InsecureRFcommSocket " + e.getMessage());
            }

            mySocket = temp;

            //Cancel discovery to prevent slow connection
            myBluetoothAdapter.cancelDiscovery();

            try {

                Log.d(TAG, "Connecting to Device: " + myDevice);
                //Blocking call and will only return on a successful connection / exception
                mySocket.connect();
                Log.d(TAG, "afterconnect " + myDevice);

                //BROADCAST CONNECTION MSG
                connectionStatusIntent = new Intent("btConnectionStatus");
                connectionStatusIntent.putExtra("ConnectionStatus", "connect");
                connectionStatusIntent.putExtra("Device", myDevice);
                LocalBroadcastManager.getInstance(myContext).sendBroadcast(connectionStatusIntent);

                Log.d(TAG, "run: ConnectThread connected");

                connected(mySocket,myDevice);
                //CANCEL ACCEPT THREAD FOR LISTENING
                if (myAcceptThread != null) {
                    myAcceptThread.cancel();
                    myAcceptThread = null;
                }

            } catch (IOException e) {

                //Close socket on error

                try {
                    mySocket.close();

                    connectionStatusIntent = new Intent("btConnectionStatus");
                    connectionStatusIntent.putExtra("ConnectionStatus", "connectionFail");
                    connectionStatusIntent.putExtra("Device", myDevice);

                    LocalBroadcastManager.getInstance(myContext).sendBroadcast(connectionStatusIntent);
                    Log.d(TAG, "run: Socket Closed: Connection Failed!! " + e.getMessage());

                } catch (IOException e1) {
                    Log.d(TAG, "myConnectThread, run: Unable to close socket connection: " + e1.getMessage());
                }

            }

        }

        public void cancel() {

            try {
                Log.d(TAG, "Cancel: Closing Client Socket");
                mySocket.close();
            } catch (IOException e) {
                Log.d(TAG, "Cancel: Closing mySocket in ConnectThread Failed " + e.getMessage());
            }
        }
    }

    //START ACCEPTTHREAD AND LISTEN FOR INCOMING CONNECTION
    public synchronized void startAcceptThread() {

        Log.d(TAG, "start");

        //Cancel any thread attempting to make a connection
        if (myConnectThread != null) {
            myConnectThread.cancel();
            myConnectThread = null;
        }
        if (myAcceptThread == null) {
            myAcceptThread = new AcceptThread();
            myAcceptThread.start();
        }
    }

    /*
    // CONNECTTHREAD STARTS AND ATTEMPT TO MAKE A CONNECTION WITH THE OTHER DEVICES
    */
    public void startClientThread(BluetoothDevice device, UUID uuid) {

        Log.d(TAG, "startClient: Started");


        myConnectThread = new ConnectThread(device, uuid);
        myConnectThread.start();

    }
private class ConnectedThread extends Thread{
    private final BluetoothSocket mSocket;
    private final InputStream inStream;
    private final OutputStream outStream;

    public ConnectedThread(BluetoothSocket socket) {
        this.mSocket = socket;
        InputStream tmpIn = null;
        OutputStream tmpOut = null;

        try {
            tmpIn = mSocket.getInputStream();
            tmpOut = mSocket.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

        inStream = tmpIn;
        outStream = tmpOut;
    }

    public void run(){
        byte[] buffer = new byte[1024];
        int bytes;

        while(true){
            try {
                bytes = inStream.read(buffer);
                String incomingMessage = new String(buffer, 0, bytes);
                Log.d(TAG, "InputStream: " + incomingMessage);

                //BROADCAST INCOMING MSG
                Intent incomingMsgIntent = new Intent("IncomingMsg");
                incomingMsgIntent.putExtra("receivingMsg", incomingMessage);
                LocalBroadcastManager.getInstance(myContext).sendBroadcast(incomingMsgIntent);

            } catch (IOException e) {
                Log.e(TAG, "Error reading input stream. "+e.getMessage());

                //BROADCAST CONNECTION MSG
                Intent connectionStatusIntent = new Intent("btConnectionStatus");
                connectionStatusIntent.putExtra("ConnectionStatus", "disconnect");
                if(myDevice != null)
                {
                    connectionStatusIntent.putExtra("Device",myDevice);
                }
                else
                {
                    connectionStatusIntent.putExtra("Device",BluetoothActivity.getBluetoothDevice());
                }

                LocalBroadcastManager.getInstance(myContext).sendBroadcast(connectionStatusIntent);

                e.printStackTrace();
                break;
            }
        }
    }

    public void write(byte[] bytes){
        String text = new String(bytes, Charset.defaultCharset());
        Log.d(TAG, "write: Writing to output stream: "+text);
        try {
            outStream.write(bytes);
        } catch (IOException e) {
            Log.e(TAG, "Error writing to output stream. "+e.getMessage());
        }
    }

    public void cancel(){
        Log.d(TAG, "cancel: Closing Client Socket");
        try{
            mSocket.close();
        } catch(IOException e){
            Log.e(TAG, "cancel: Failed to close ConnectThread mSocket " + e.getMessage());
        }
    }
}

    private void connected(BluetoothSocket mSocket, BluetoothDevice device) {
        Log.d(TAG, "connected: Starting.");
        myDevice =  device;
        if (myAcceptThread != null) {
            myAcceptThread.cancel();
            myAcceptThread = null;
        }

        mConnectedThread = new ConnectedThread(mSocket);
        mConnectedThread.start();
        //save connected device's mac address
        if(device != null)
        {
            PrefManager.write(LAST_MAC , device.getAddress());;
        }

    }

    public static void write(byte[] out){

        Log.d(TAG, "write: Write is called." );
        if(mConnectedThread != null)
        {
            mConnectedThread.write(out);
        }

    }
}