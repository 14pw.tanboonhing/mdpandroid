package com.example.android.mdpandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ReconfigureActivity extends AppCompatActivity {
    Button savePrefBtn;
    CommandPref commands;
    EditText functionOneEdit , functionTwoEdit, forwardEdit , reverseEdit , strafeLeftEdit , strafeRightEdit , rotateRightEdit, rotateLeftEdit ,  beginExploreEdit ,beginFastestPathEdit , sendAreaInfoEdit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reconfigure);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        commands = CommandPref.getInstance(getApplicationContext());
        findViewByIdes();
        initCommandTexts();
        implementListeners();
    }

    private void implementListeners() {
        savePrefBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commands.setAllCommands(functionOneEdit.getText().toString() , functionTwoEdit.getText().toString(), forwardEdit.getText().toString() , reverseEdit.getText().toString() , "",
                        "" , rotateRightEdit.getText().toString(), rotateLeftEdit.getText().toString() ,  beginExploreEdit.getText().toString() ,beginFastestPathEdit.getText().toString() , sendAreaInfoEdit.getText().toString());
                Toast.makeText(getApplicationContext(), "Preferences Saved!",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initCommandTexts()
    {
        functionOneEdit.setText(commands.getFunctionOne());
        functionTwoEdit.setText(commands.getFunctionTwo());
        forwardEdit.setText(commands.getForward());
        reverseEdit.setText(commands.getReverse());
//        strafeLeftEdit.setText(commands.getStrafeLeft());
//        strafeRightEdit.setText(commands.getStrafeRight());
        rotateLeftEdit.setText(commands.getRotateLeft());
        rotateRightEdit.setText(commands.getRotateRight());
        beginFastestPathEdit.setText(commands.getBeginFastPath());
        beginExploreEdit.setText(commands.getBeginExplore());
        sendAreaInfoEdit.setText(commands.getSendArenaInfo());
    }

    private void findViewByIdes() {
        functionOneEdit = findViewById(R.id.function_one_text_input);
        functionTwoEdit = findViewById(R.id.function_two_text_input);
        forwardEdit = findViewById(R.id.forward_text_input);
        reverseEdit = findViewById(R.id.reverse_text_input);
//        strafeLeftEdit = findViewById(R.id.strafe_left_input);
//        strafeRightEdit = findViewById(R.id.strafe_right_input);
        rotateLeftEdit = findViewById(R.id.rotate_left_input);
        rotateRightEdit = findViewById(R.id.rotate_right_input);
        beginFastestPathEdit = findViewById(R.id.begin_fastest_path_text_input);
        beginExploreEdit = findViewById(R.id.begin_exploration_text_input);
        sendAreaInfoEdit = findViewById(R.id.send_arena_info_text_input);
        savePrefBtn = findViewById(R.id.save_preferences_btn);
    }
}
