package com.example.android.mdpandroid;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class PageAdapter extends FragmentPagerAdapter {
    private int numOfTabs;
    private MessagingFragment msgFragment;

    public PageAdapter(@NonNull FragmentManager fm, int behavior , int numOfTabs) {
        super(fm, behavior);
        Log.d("debug" , "page");
        this.numOfTabs = numOfTabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Log.d("debug" , "getitem");
        switch (position){
            case 0 :
                return new ControlsFragment();
            case 1 :
                msgFragment = new MessagingFragment();
                return msgFragment;
            case 2 :
                return new MapFragment();
            default:
                return null;
        }
    }

    public MessagingFragment getMsgFragment() {
        return msgFragment;
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        super.getPageTitle(position);

        switch (position) {
            case 0:
                return "Controls";
            case 1:
                return "Messaging";
            case 2:
                return "Map";

            default:
                return null;
        }
    }
}
