package com.example.android.mdpandroid;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.ToggleButton;

import views.GridMazeView;


/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends Fragment {
    CommandPref commands;
    private CheckBox setObstacleBtn , setExploredBtn , setUnExploredBtn , setWayPointBtn , setStartPointBtn , setRobotPosBtn;
    Switch autoUpdateSwitch;
    Button manulUpdateButton , resetMapBtn , prevMapBtn;
    ToggleButton imgOnlyObsBtn;
    GridMazeView gridMazeView;
    AlertDialog resetMapDialog;
    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        commands = CommandPref.getInstance(getActivity());
        findViewByIdes(view);
        autoUpdateSwitch.setChecked(true);
        implementListeners();
        initResetMapDialog();

    }
    private void initResetMapDialog(){
        resetMapDialog = new AlertDialog.Builder(getContext()).create();
        resetMapDialog.setTitle("Map Reset");
        resetMapDialog.setMessage("Are you sure you want to reset map?");
        resetMapDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        gridMazeView.reSetGridMaze();
                        gridMazeView.reSetMazePref();
                        gridMazeView.postInvalidate();
                    }
                });
        resetMapDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });

    }
    private void implementListeners() {
        prevMapBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gridMazeView.getMDFfromPref();
                //real maze use this
                gridMazeView.createMazeFromMDF();
                //use this for amd tool because amd map upside down
                //gridMazeView.createMazeFromMdfAMD();
            }
        });
        resetMapBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetMapDialog.show();
            }
        });
        manulUpdateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).setManualUpdate(true);
                BluetoothConnectionService.write(commands.getSendArenaInfo().getBytes());
            }
        });
        autoUpdateSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ((MainActivity)getActivity()).setAutoUpdateMap(isChecked);
            }
        });
        setObstacleBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    setRobotPosBtn.setChecked(false);
                    setExploredBtn.setChecked(false);
                    setUnExploredBtn.setChecked(false);
                    setWayPointBtn.setChecked(false);
                    setStartPointBtn.setChecked(false);
                    gridMazeView.setCurrentContentToChange(GridMazeView.GRID_OBSTACLE);
                } else {
                    // The toggle is disabled
                    gridMazeView.disableCurrentContentToChange(GridMazeView.GRID_OBSTACLE);
                }
            }
        });

        setExploredBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    setRobotPosBtn.setChecked(false);
                    setObstacleBtn.setChecked(false);
                    setUnExploredBtn.setChecked(false);
                    setWayPointBtn.setChecked(false);
                    setStartPointBtn.setChecked(false);
                    gridMazeView.setCurrentContentToChange(GridMazeView.GRID_EXPLORED);
                } else {
                    // The toggle is disabled
                    gridMazeView.disableCurrentContentToChange(GridMazeView.GRID_EXPLORED);
                }
            }
        });

        setUnExploredBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    setRobotPosBtn.setChecked(false);
                    setObstacleBtn.setChecked(false);
                    setExploredBtn.setChecked(false);
                    setWayPointBtn.setChecked(false);
                    setStartPointBtn.setChecked(false);
                    gridMazeView.setCurrentContentToChange(GridMazeView.GRID_UNEXPLORED);
                } else {
                    // The toggle is disabled
                    gridMazeView.disableCurrentContentToChange(GridMazeView.GRID_UNEXPLORED);
                }
            }
        });

        setStartPointBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    setRobotPosBtn.setChecked(false);
                    setObstacleBtn.setChecked(false);
                    setExploredBtn.setChecked(false);
                    setUnExploredBtn.setChecked(false);
                    setWayPointBtn.setChecked(false);
                    gridMazeView.setCurrentContentToChange(GridMazeView.GRID_STARTPOINT);
                } else {
                    // The toggle is disabled
                    gridMazeView.disableCurrentContentToChange(GridMazeView.GRID_STARTPOINT);
                }
            }
        });

        setWayPointBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    setRobotPosBtn.setChecked(false);
                    setObstacleBtn.setChecked(false);
                    setExploredBtn.setChecked(false);
                    setUnExploredBtn.setChecked(false);
                    setStartPointBtn.setChecked(false);
                    gridMazeView.setCurrentContentToChange(GridMazeView.GRID_WAYPOINT);
                } else {
                    // The toggle is disabled
                    gridMazeView.disableCurrentContentToChange(GridMazeView.GRID_WAYPOINT);
                }
            }
        });
        setRobotPosBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    setWayPointBtn.setChecked(false);
                    setObstacleBtn.setChecked(false);
                    setExploredBtn.setChecked(false);
                    setUnExploredBtn.setChecked(false);
                    setStartPointBtn.setChecked(false);
                    gridMazeView.setCurrentContentToChange(GridMazeView.GRID_ROBOTPOS);
                } else {
                    // The toggle is disabled
                    gridMazeView.disableCurrentContentToChange(GridMazeView.GRID_ROBOTPOS);
                }
            }
        });
        imgOnlyObsBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                gridMazeView.setShowImgOnly(isChecked);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_map, container, false);
    }

    private void findViewByIdes(View view)
    {
        imgOnlyObsBtn = view.findViewById(R.id.img_only_obs_btn);
        prevMapBtn = view.findViewById(R.id.previous_map_btn);
        resetMapBtn = view.findViewById(R.id.reset_map_btn);
        manulUpdateButton = view.findViewById(R.id.manual_update_btn);
        autoUpdateSwitch = view.findViewById(R.id.auto_update_switch);
        gridMazeView = ((MainActivity)getActivity()).getGridMazeView();
        setObstacleBtn = view.findViewById(R.id.set_obs_btn);
        setExploredBtn = view.findViewById(R.id.set_explore_btn);
        setUnExploredBtn = view.findViewById(R.id.set_unexplored_btn);
        setWayPointBtn = view.findViewById(R.id.set_waypoint_btn);
        setStartPointBtn = view.findViewById(R.id.set_startpoint_btn);
        setRobotPosBtn = view.findViewById(R.id.set_robot_pos);
    }
}
