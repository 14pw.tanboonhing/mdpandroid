package com.example.android.mdpandroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.UUID;

import views.GridMazeView;

public class MainActivity extends AppCompatActivity {
    Button printMdfBtn , printImgBtn;
    TabLayout tabLayout;
    PageAdapter pageAdapter;
    BluetoothAdapter mBluetoothAdapter;
    TabItem tabMessaging , tabControls , tabMap;
    ViewPager viewPager;
    GridMazeView gridMazeView;
    TextView robotStatusTxt;
    private boolean autoUpdateMap = true;
    private boolean manualUpdate = false;
    ClipboardManager myClipboard;

    @Override
    protected void onStop() {
        super.onStop();
        PrefManager.write(BluetoothActivity.CONNECTION_STATUS,"Connection Status");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewByIdes();
        pageAdapter = new PageAdapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT,tabLayout.getTabCount());
        viewPager.setAdapter(pageAdapter);
        tabLayout.setupWithViewPager(viewPager);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        myClipboard = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
        implementListeners();


        //REGISTER BROADCAST RECEIVER FOR CONNECTION STATUS
        LocalBroadcastManager.getInstance(this).registerReceiver(btConnectionReceiver, new IntentFilter("btConnectionStatus"));
        //REGISTER BROADCAST RECEIVER FOR INCOMING MSG
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver, new IntentFilter("IncomingMsg"));

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.bluetooth_menu:
                Intent intent = new Intent(getApplicationContext(), BluetoothActivity.class);
                startActivity(intent);
                return true;
            case R.id.reconfigure_menu:
                Intent intent1 = new Intent(getApplicationContext(), ReconfigureActivity.class);
                startActivity(intent1);
                return true;
            case R.id.reconnect_bluetooth:
                String macLast = PrefManager.read(BluetoothConnectionService.LAST_MAC , "");
                if(macLast == "")
                {
                    return true;
                }
                BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(macLast);
                startBTConnection(device , BluetoothActivity.MY_UUID);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mdp_menu, menu);
        return true;
    }

    private void findViewByIdes()
    {
        printImgBtn = findViewById(R.id.print_img_btn);
        printMdfBtn = findViewById(R.id.print_mdf_btn);
        robotStatusTxt = findViewById(R.id.robot_status);
        gridMazeView = findViewById(R.id.gridMazeView);
        tabLayout = findViewById(R.id.tabLayout);
        tabControls = findViewById(R.id.controlsTab);
        tabMap = findViewById(R.id.mapTab);
        tabMessaging = findViewById(R.id.messagingTab);
        viewPager = findViewById(R.id.viewPager);
    }

    public GridMazeView getGridMazeView(){
        return gridMazeView;
    }

    public void setAutoUpdateMap(boolean autoUpdateMap)
    {
        this.autoUpdateMap = autoUpdateMap;
        gridMazeView.setAutoUpdate(autoUpdateMap);
    }
    public void setManualUpdate(boolean manualUpdate)
    {
        this.manualUpdate = manualUpdate;
    }

    private void implementListeners(){
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        printMdfBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "Exploration:" + gridMazeView.getExplorationString() +"\n" + "Obstacle:" + gridMazeView.getObstacleString();
                pageAdapter.getMsgFragment().setTextBox(msg);
                myClipboard.setPrimaryClip(ClipData.newPlainText("MDF String", msg));
                Toast.makeText(getApplicationContext(),"MDF String copied to clipboard" , Toast.LENGTH_SHORT).show();
                Log.d("debug", msg);
            }
        });
        printImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "IMG string:" + gridMazeView.getImageString();
                pageAdapter.getMsgFragment().setTextBox(msg);
                myClipboard.setPrimaryClip(ClipData.newPlainText("Image String", msg));
                Toast.makeText(getApplicationContext(),"IMG String copied to clipboard" , Toast.LENGTH_SHORT).show();
                Log.d("debug", msg);
            }
        });
    }
    //BROADCAST RECEIVER FOR INCOMING MESSAGE
    BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Log.d("debug", "Receiving Msg!!!");

            String msg = intent.getStringExtra("receivingMsg");
            Log.d("debug", "message: "+msg);
            if(msg == null || msg.length() ==0)
            {
                Log.d("debug", "null Msg!!!");
                return;
            }
            if(msg.equals("endExp"))
            {
                String imgString = "j" + gridMazeView.getImageStringForImageTile();
                BluetoothConnectionService.write(imgString.getBytes());
            }
            pageAdapter.getMsgFragment().setTextBox(msg);
            try {
                JSONObject reader = new JSONObject(msg);

                JSONArray ObjectKeyNames = reader.names();
                if(ObjectKeyNames != null)
                {
                    for(int i = 0; i < ObjectKeyNames.length();i++)
                    {
                        switch (ObjectKeyNames.getString(i))
                        {
                            case "grid":
                                if(autoUpdateMap || manualUpdate)
                                {
                                    manualUpdate = false;
                                    String explorationString = "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff";
                                    String obstacleString = "0" + reader.getString("grid");
                                    gridMazeView.setExplorationString(explorationString);
                                    gridMazeView.setObstacleString(obstacleString);
                                    gridMazeView.createMazeFromMdfAMD();
                                }
                                break;
                            case"map":
                                if(autoUpdateMap || manualUpdate)
                                {
                                    manualUpdate = false;
                                    JSONObject mapInfo = reader.getJSONObject("map");
                                    String mapExplorationString = mapInfo.getString("exploration");
                                    String mapObstacleString =mapInfo.getString("obstacle");
                                    gridMazeView.setExplorationString(mapExplorationString);
                                    gridMazeView.setObstacleString(mapObstacleString);
                                    gridMazeView.createMazeFromMDF();

                                    JSONArray robotPos = mapInfo.getJSONArray("robotPos");
                                    gridMazeView.setRobotPos(robotPos.getInt(0) -1 , robotPos.getInt(1) -1, robotPos.getString(2));
                                    Log.d("debug" , "x: " + robotPos.getInt(0) + " y: " +robotPos.getInt(1) + " direct: " + robotPos.getString(2));
                                }
                                break;
                            case "status":
                                String status = reader.getString("status");
                                robotStatusTxt.setText(status);
                                break;
                            case "image":
                                JSONArray image = reader.getJSONArray("image");
                                gridMazeView.addNumberIdBlocks(image.getInt(0) , image.getInt(1) , image.getInt(2));
                                break;
                            case "robotPosition": // for amd tool
                                JSONArray robotPosition = reader.getJSONArray("robotPosition");
                                gridMazeView.setRobotPos(robotPosition.getInt(0) , robotPosition.getInt(1) , robotPosition.getInt(2));
                                Log.d("debug" , "x: " + robotPosition.getInt(0) + " y: " +robotPosition.getInt(1) + " direct: " + robotPosition.getInt(2));
                                break;

                        }
                    }
                }



//                for (int i = 0; i < reader.names().length(); i++) {
//                    switch (reader.names().getString(i)) {
//
//                    }
//                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };
    //BROADCAST RECEIVER FOR BLUETOOTH CONNECTION STATUS
    BroadcastReceiver btConnectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("debug", "Receiving connection status!!!");
            String connectionStatus = intent.getStringExtra("ConnectionStatus");
            BluetoothDevice myBTDevice = intent.getParcelableExtra("Device");
            String deviceName = "";
            if(myBTDevice != null)
            {
                deviceName =  myBTDevice.getName();
            }
            String toastText="";
            //DISCONNECTED FROM BLUETOOTH CHAT
            if(connectionStatus.equals("disconnect")){
                startAcceptBTConnection();
                toastText = "Device disconnected: " + deviceName;
                PrefManager.write(BluetoothActivity.CONNECTION_STATUS,"disconnected");
            }
            //SUCCESSFULLY CONNECTED TO BLUETOOTH DEVICE
            else if(connectionStatus.equals("connect")){
                toastText = "Device connected: " + deviceName;
                PrefManager.write(BluetoothActivity.CONNECTION_STATUS,"Connected to "+ myBTDevice.getName());
            }
            //BLUETOOTH CONNECTION FAILED
            else if(connectionStatus.equals("connectionFail")) {
                toastText = "Connection failed" + deviceName;
                PrefManager.write(BluetoothActivity.CONNECTION_STATUS,"connection failed");
            }
            Toast.makeText(getApplicationContext(),toastText , Toast.LENGTH_LONG).show();
        }
    };
    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(btConnectionReceiver);
    }

    public void startAcceptBTConnection()
    {
        if(BluetoothActivity.connectIntent != null) {
            //Stop Bluetooth Connection Service
            stopService(BluetoothActivity.connectIntent);
        }
        //START BLUETOOTH CONNECTION SERVICE WHICH WILL START THE ACCEPTTHREAD TO LISTEN FOR CONNECTION
        BluetoothActivity.connectIntent = new Intent(MainActivity.this, BluetoothConnectionService.class);
        BluetoothActivity.connectIntent.putExtra("serviceType", "listen");
        startService(BluetoothActivity.connectIntent);
    }
    public void startBTConnection(BluetoothDevice device, UUID uuid) {

        Log.d("debug", "StartBTConnection: Initializing RFCOM Bluetooth Connection");

        if(BluetoothActivity.connectIntent != null) {
            //Stop Bluetooth Connection Service
            stopService(BluetoothActivity.connectIntent);
        }
        BluetoothActivity.connectIntent = new Intent(MainActivity.this, BluetoothConnectionService.class);
        BluetoothActivity.connectIntent.putExtra("serviceType", "connect");
        BluetoothActivity.connectIntent.putExtra("device", device);
        BluetoothActivity.connectIntent.putExtra("id", uuid);
        Log.d("debug", "NAME: " + device.getName());
        Log.d("debug", "StartBTConnection: Starting Bluetooth Connection Service!");

        startService(BluetoothActivity.connectIntent);
    }

}
