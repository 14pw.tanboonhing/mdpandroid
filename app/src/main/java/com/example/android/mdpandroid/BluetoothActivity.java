package com.example.android.mdpandroid;


import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

public class BluetoothActivity extends AppCompatActivity {


    public static final String CONNECTION_STATUS = "CONNECTION_STATUS";
    Button btnListen, btnListDevices;
    TextView connectionStatusTxt;
    ListView scanListView;
    BluetoothAdapter bluetoothAdapter;
    Set<BluetoothDevice> devicesArray;
    ArrayList<String> pairedDevices = new ArrayList<String>();
    ArrayList<BluetoothDevice> devices = new ArrayList<BluetoothDevice>();
    ArrayAdapter<String> arrayAdapter;
    String TAG = "debug";
    public static Intent connectIntent;
    AlertDialog alertDialog;
    static BluetoothDevice myBTDevice;
    private String connectionStatus;
    int REQUEST_ENABLE_BLUETOOTH=1;

    public static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        findViewByIdes();
        alertDialog = new AlertDialog.Builder(BluetoothActivity.this).create();
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(!bluetoothAdapter.isEnabled())
        {
            Intent btEnablingIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(btEnablingIntent,REQUEST_ENABLE_BLUETOOTH);
        }

        getPairedDevices();
        implementListeners();
        //REGISTER BROADCAST RECEIVER FOR CONNECTION STATUS
        LocalBroadcastManager.getInstance(this).registerReceiver(btConnectionReceiver, new IntentFilter("btConnectionStatus"));
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(receiver, filter);
        //REGISTER DISCOVERABILITY BROADCAST RECEIVER
        IntentFilter intentFilter = new IntentFilter(bluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
        registerReceiver(discoverabilityBroadcastReceiver, intentFilter);
        arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1,0);
        scanListView.setAdapter(arrayAdapter);
        connectionStatus = PrefManager.read(CONNECTION_STATUS,"Not Connected");
        connectionStatusTxt.setText(connectionStatus);
        listBTDevices();
    }
    //BROADCAST RECEIVER FOR BLUETOOTH CONNECTION STATUS
    BroadcastReceiver btConnectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {


            String connectionStatus = intent.getStringExtra("ConnectionStatus");
            myBTDevice = intent.getParcelableExtra("Device");
            Log.d(TAG, "Receiving btConnectionStatus Msg!!!" + connectionStatus );

            //DISCONNECTED FROM BLUETOOTH DEVICE
            if(connectionStatus.equals("disconnect")){

                Log.d("debug:","Device Disconnected");
                connectionStatus = "disconnected";
                PrefManager.write(CONNECTION_STATUS,connectionStatus);
                connectionStatusTxt.setText(connectionStatus);
                startAcceptBTConnection();
                //RECONNECT DIALOG MSG

                alertDialog.setTitle("BLUETOOTH DISCONNECTED");
                alertDialog.setMessage("Connection with device: '"+myBTDevice.getName()+"' has ended. Do you want to reconnect?");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                startBTConnection(myBTDevice, MY_UUID);

                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }

            //SUCCESSFULLY CONNECTED TO BLUETOOTH DEVICE
            else if(connectionStatus.equals("connect")){


                Log.d("ConnectActivity:","Device Connected");
                Toast.makeText(BluetoothActivity.this, "Connection Established: "+ myBTDevice.getName(),
                        Toast.LENGTH_LONG).show();
                connectionStatus = "Connected to "+ myBTDevice.getName();
                PrefManager.write(CONNECTION_STATUS,connectionStatus);
                connectionStatusTxt.setText(connectionStatus);
                if(alertDialog.isShowing())
                {
                    alertDialog.dismiss();
                }

            }

            //BLUETOOTH CONNECTION FAILED
            else if(connectionStatus.equals("connectionFail")) {
                Toast.makeText(BluetoothActivity.this, "Connection Failed: "+ myBTDevice.getName(),
                        Toast.LENGTH_LONG).show();
                connectionStatus = "connection failed";
                PrefManager.write(CONNECTION_STATUS,connectionStatus);
                connectionStatusTxt.setText(connectionStatus);
            }

        }
    };
    public static BluetoothDevice getBluetoothDevice(){
        return myBTDevice;
    }

    public void startBTConnection(BluetoothDevice device, UUID uuid) {

        Log.d("debug", "StartBTConnection: Initializing RFCOM Bluetooth Connection");

        if(connectIntent != null) {
            //Stop Bluetooth Connection Service
            stopService(connectIntent);
        }
        connectIntent = new Intent(BluetoothActivity.this, BluetoothConnectionService.class);
        connectIntent.putExtra("serviceType", "connect");
        connectIntent.putExtra("device", device);
        connectIntent.putExtra("id", uuid);
        Log.d("debug", "NAME: " + device.getName());
        Log.d("debug", "StartBTConnection: Starting Bluetooth Connection Service!");

        startService(connectIntent);
    }
    public void startAcceptBTConnection()
    {
        if(connectIntent != null) {
            //Stop Bluetooth Connection Service
            stopService(connectIntent);
        }
        //START BLUETOOTH CONNECTION SERVICE WHICH WILL START THE ACCEPTTHREAD TO LISTEN FOR CONNECTION
        connectIntent = new Intent(BluetoothActivity.this, BluetoothConnectionService.class);
        connectIntent.putExtra("serviceType", "listen");
        startService(connectIntent);
    }
    /*
        Create a BroadcastReceiver for ACTION_FOUND (Enable Discoverability).
    */
    private final BroadcastReceiver discoverabilityBroadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (action.equals(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED)) {
                int mode = intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE, BluetoothAdapter.ERROR);

                switch (mode) {
                    //DEVICE IS IN DISCOVERABLE MODE
                    case BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE:
                        Log.d(TAG, "OnReceiver: DISCOVERABILITY ENABLED");

                        startAcceptBTConnection();


                        break;
                    //DEVICE IS NOT IN DISCOVERABLE MODE
                    case BluetoothAdapter.SCAN_MODE_CONNECTABLE:
                        Log.d(TAG, "OnReceiver: DISCOVERABILITY DISABLED, ABLE TO RECEIVE CONNECTION");
                        break;
                    //BLUETOOTH TURNING ON STATE
                    case BluetoothAdapter.SCAN_MODE_NONE:
                        Log.d(TAG, "OnReceiver: DISCOVERABILITY DISABLED, NOT ABLE TO RECEIVE CONNECTION");
                        break;
                    //BLUETOOTH TURNED ON STATE
                    case BluetoothAdapter.STATE_CONNECTING:
                        Log.d(TAG, "OnReceiver: CONNECTING");
                        break;
                    //BLUETOOTH TURNED ON STATE
                    case BluetoothAdapter.STATE_CONNECTED:
                        Log.d(TAG, "OnReceiver: CONNECTED");
                        break;
                }
            }
        }
    };
    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Discovery has found a device. Get the BluetoothDevice
                // object and its info from the Intent.


                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                Log.i("tag","new device" + device.getName());
                if(device.getName() != null && !devices.contains(device))
                {
                    devices.add(device);
                    String s = "";
                    if(pairedDevices.contains(device.getAddress()))
                    {
                        s = "(Paired)";
                    }
                    arrayAdapter.add(device.getName()+" "+s+" "+"\n"+device.getAddress());
                    arrayAdapter.notifyDataSetChanged();
                    Log.i("tag","added new device" + device.getName());
                }

            }
        }
    };
    /*
            TURN DISCOVERABILITY ON
        */
    private void discoverabilityON() {

        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 900);
        startActivity(discoverableIntent);
    }
    /*
     Check BT permission in manifest (For Start Discovery)
 */
    private void checkBTPermission() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            int permissionCheck = ContextCompat.checkSelfPermission(getApplicationContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION);

            permissionCheck += ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION);
            if (permissionCheck != 0) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001);
            }
        } else {
            Log.d(TAG, "checkBTPermissions: No need to check permissions. SDK version < LOLLIPOP.");

        }
    }


    /*
      START DISCOVERING OTHER DEVICES
  */
    private void startDiscovery() {
        Log.d(TAG, "btnDiscover: Looking for unpaired devices.");

        //DISCOVER OTHER DEVICES
        if (bluetoothAdapter.isDiscovering()) {
            bluetoothAdapter.cancelDiscovery();
            Log.d(TAG, "BTDiscovery: canceling discovery");

            //check BT permission in manifest
            checkBTPermission();

            bluetoothAdapter.startDiscovery();
            Log.d(TAG, "BTDiscovery: enable discovery");


        }
        if (!bluetoothAdapter.isDiscovering()) {

            //check BT permission in manifest
            checkBTPermission();

            bluetoothAdapter.startDiscovery();
            Log.d(TAG, "BTDiscovery: enable discovery");


        }
    }
    private void getPairedDevices() {
        devicesArray = bluetoothAdapter.getBondedDevices();
        if(devicesArray.size()>0){
            for(BluetoothDevice device:devicesArray){
                pairedDevices.add(device.getAddress());
            }
        }
    }
    private void listBTDevices(){
        pairedDevices.clear();
        getPairedDevices();
        devices.clear();
        arrayAdapter.clear();
        startDiscovery();
    }
    private void implementListeners() {
        btnListDevices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listBTDevices();
            }
        });
        btnListen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                discoverabilityON();
            }
        });
        scanListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.i("debug" , "name: " + devices.get(i).getName());
                myBTDevice = devices.get(i);
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2 && myBTDevice.getBondState() == BluetoothDevice.BOND_NONE) {
                    Log.d(TAG, "Rebond");
                    Toast.makeText(BluetoothActivity.this, "Bonding with device: "+ myBTDevice.getName(),
                            Toast.LENGTH_SHORT).show();
                    myBTDevice.createBond();
                    startBTConnection(myBTDevice,MY_UUID);
                }
                else
                {
                    Toast.makeText(BluetoothActivity.this, "Connecting with device: "+ myBTDevice.getName(),
                            Toast.LENGTH_SHORT).show();
                    startBTConnection(myBTDevice,MY_UUID);

                }

                connectionStatusTxt.setText("Connecting");
            }
        });

    }


    private void findViewByIdes()
    {
        scanListView = (ListView) findViewById(R.id.device_list_view);
        btnListen = (Button) findViewById(R.id.listenBtn);
        btnListDevices = (Button) findViewById(R.id.listDeviceBtn);
        connectionStatusTxt = (TextView) findViewById(R.id.connection_status);
    }
    @Override
    protected void onDestroy() {
        Log.d(TAG, "ConnectActivity: onDestroyed: destroyed");
        super.onDestroy();
        unregisterReceiver(receiver);
        unregisterReceiver(discoverabilityBroadcastReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(btConnectionReceiver);

    }
}
