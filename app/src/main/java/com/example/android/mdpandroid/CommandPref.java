package com.example.android.mdpandroid;

import android.content.Context;

public class CommandPref {
    private final String FUNCTION_ONE = "FUNCTION_ONE" , FUNCTION_TWO = "FUNCTION_TWO";
    private final String FORWARD = "FORWARD",REVERSE = "REVERSE" , STRAFE_LEFT = "STRAFE_LEFT",STRAFE_RIGHT = "STRAFE_RIGHT" , ROTATE_RIGHT = "ROTATE_RIGHT" , ROTATE_LEFT = "ROTATE_LEFT";
    private final String BEGIN_EXPLORATION = "BEGIN_EXPLORATION" , BEGIN_FASTEST_PATH = "BEGIN_FASTEST_PATH" ,SEND_ARENA_INFO = "SEND_ARENA_INFO";

    private String functionOne , functionTwo , forward , reverse , strafeLeft , strafeRight , rotateRight , rotateLeft , beginExplore , beginFastPath , sendArenaInfo;
    private static CommandPref commandPref_instance = null;
    private CommandPref(Context context){
        PrefManager.init(context);
        init();
    }
    public static CommandPref getInstance(Context context)
    {
        if (commandPref_instance == null)
            commandPref_instance = new CommandPref(context);

        return commandPref_instance;
    }
    private void init()
    {
        functionOne = PrefManager.read(FUNCTION_ONE,"function one");
        functionTwo = PrefManager.read(FUNCTION_TWO,"function two");
        forward = PrefManager.read(FORWARD,"f");
        reverse = PrefManager.read(REVERSE,"r");
        strafeLeft = PrefManager.read(STRAFE_LEFT,"sl");
        strafeRight = PrefManager.read(STRAFE_RIGHT,"sr");
        rotateRight = PrefManager.read(ROTATE_RIGHT,"tr");
        rotateLeft = PrefManager.read(ROTATE_LEFT,"tl");
        beginExplore = PrefManager.read(BEGIN_EXPLORATION,"beginExplore");
        beginFastPath = PrefManager.read(BEGIN_FASTEST_PATH,"beginFastest");
        sendArenaInfo = PrefManager.read(SEND_ARENA_INFO,"sendArena");
    }
    public void setAllCommands(String functionOne , String functionTwo , String forward , String reverse , String strafeLeft , String strafeRight ,String rotateRight ,String rotateLeft ,String beginExplore ,String beginFastPath ,String sendArenaInfo)
    {
        setFunctionOne(functionOne);
        setFunctionTwo(functionTwo);
        setForward(forward);
        setReverse(reverse);
        setStrafeLeft(strafeLeft);
        setStrafeRight(strafeRight);
        setRotateRight(rotateRight);
        setRotateLeft(rotateLeft);
        setBeginExplore(beginExplore);
        setBeginFastPath(beginFastPath);
        setSendArenaInfo(sendArenaInfo);
    }

    public String getFunctionOne() {
        return functionOne;
    }

    public void setFunctionOne(String functionOne) {
        if(functionOne.length()>0 && functionOne != this.functionOne)
        {
            this.functionOne = functionOne;
            PrefManager.write(FUNCTION_ONE,functionOne);
        }
    }

    public String getFunctionTwo() {
        return functionTwo;
    }

    public void setFunctionTwo(String functionTwo) {

        if(functionTwo.length()>0 && functionTwo != this.functionTwo)
        {
            this.functionTwo = functionTwo;
            PrefManager.write(FUNCTION_TWO,functionTwo);
        }
    }

    public String getForward() {
        return forward;
    }

    public void setForward(String forward) {
        if(forward.length()>0 && forward != this.forward)
        {
            this.forward = forward;
            PrefManager.write(FORWARD,forward);
        }
    }

    public String getReverse() {
        return reverse;
    }

    public void setReverse(String reverse) {
        if(reverse.length()>0 && reverse != this.reverse)
        {
            this.reverse = reverse;
            PrefManager.write(REVERSE,reverse);
        }
    }

    public String getStrafeLeft() {
        return strafeLeft;
    }

    public void setStrafeLeft(String strafeLeft) {
        if(strafeLeft.length()>0 && strafeLeft != this.strafeLeft)
        {
            this.strafeLeft = strafeLeft;
            PrefManager.write(STRAFE_LEFT,strafeLeft);
        }
    }

    public String getStrafeRight() {
        return strafeRight;
    }

    public void setStrafeRight(String strafeRight) {
        if(strafeRight.length()>0 && strafeRight != this.strafeRight)
        {
            this.strafeRight = strafeRight;
            PrefManager.write(STRAFE_RIGHT,strafeRight);
        }
    }

    public String getRotateRight() {
        return rotateRight;
    }

    public void setRotateRight(String rotateRight) {
        if(rotateRight.length()>0 && rotateRight != this.rotateRight)
        {
            this.rotateRight = rotateRight;
            PrefManager.write(ROTATE_RIGHT,rotateRight);
        }
    }

    public String getRotateLeft() {
        return rotateLeft;
    }

    public void setRotateLeft(String rotateLeft) {
        if(rotateLeft.length()>0 && rotateLeft != this.rotateLeft)
        {
            this.rotateLeft = rotateLeft;
            PrefManager.write(ROTATE_LEFT,rotateLeft);
        }
    }

    public String getBeginExplore() {
        return beginExplore;
    }

    public void setBeginExplore(String beginExplore) {
        if(beginExplore.length()>0 && beginExplore != this.beginExplore)
        {
            this.beginExplore = beginExplore;
            PrefManager.write(BEGIN_EXPLORATION,beginExplore);
        }
    }

    public String getBeginFastPath() {
        return beginFastPath;
    }

    public void setBeginFastPath(String beginFastPath) {
        if(beginFastPath.length()>0 && beginFastPath != this.beginFastPath)
        {
            this.beginFastPath = beginFastPath;
            PrefManager.write(BEGIN_FASTEST_PATH,beginFastPath);
        }
    }

    public String getSendArenaInfo() {
        return sendArenaInfo;
    }

    public void setSendArenaInfo(String sendArenaInfo) {
        if(sendArenaInfo.length()>0 && sendArenaInfo != this.sendArenaInfo)
        {
            this.sendArenaInfo = sendArenaInfo;
            PrefManager.write(SEND_ARENA_INFO,sendArenaInfo);
        }
    }
}
