package views;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.graphics.Matrix;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;

import com.example.android.mdpandroid.BluetoothConnectionService;
import com.example.android.mdpandroid.PrefManager;
import com.example.android.mdpandroid.R;

import java.math.BigInteger;
import java.util.ArrayList;

public class GridMazeView extends View {
    private static final String EXPLORATION_STRING = "EXPLORATION_STRING" , OBSTACLE_STRING = "OBSTACLE_STRING" ,ROBOT_POSITION_STRING = "ROBOT_POSITION_STRING" , IMAGE_STRING = "IMAGE_STRING";
    //maze is 15 x 20
    private int squareSize = 25 , topGridPadding = 10, leftGridPadding = 20;
    private String explorationString ="" , obstacleString = "";
    private final int gridWidth =  15, gridHeight = 20 ;
    private Rect[][] squareRect;
    //private Rect startPointRect;
    private int[][] gridContent;
    public static final int GRID_UNEXPLORED = 0 , GRID_EXPLORED = 1 , GRID_OBSTACLE = 2 , GRID_WAYPOINT = 3 , GRID_STARTPOINT = 4, GRID_ROBOTPOS = 5;
    private int pressedWayPointX = -1, pressedWaypointY=-1, wayPointX = -1, wayPointY = -1, startPointX = -1, startPointY = -1 , robotPosX = -1 , robotPosY = -1 , rotation = 0;//robotPos from top left of robot
    private Paint unexploredPaint , exploredPaint , obstaclePaint , wayPointPaint , gridNumberPaint;//, startPointPaint;
    private Paint strokePaint;
    private int currentContentToChange;
    private boolean autoUpdate = true;
    private Bitmap robotBitmap0, robotBitmap90 , robotBitmap180 , robotBitmap270 ;
    private Bitmap[] idBlocksBitmap;
    private int numOfIdBlocks = 15;
    private int robotWidth = 3 , robotHeight = 3;
    private ArrayList<IdBlock> idBlocks;
    private String idBlocksString = "";
    AlertDialog alertDialog;
    private boolean showImgOnly = true;
    public GridMazeView(Context context) {
        super(context);
        init(null);
    }

    private void init(@Nullable AttributeSet set) {
        alertDialog = new AlertDialog.Builder(getContext()).create();
        idBlocksBitmap = new Bitmap[numOfIdBlocks];
        idBlocks = new ArrayList<>();
        squareSize = (int)convertDpToPixel(squareSize, getContext());
        topGridPadding = (int)convertDpToPixel(topGridPadding , getContext());
        leftGridPadding = (int)convertDpToPixel(leftGridPadding , getContext());
        currentContentToChange = -1;

        initBitmaps();
        initPaint();
        squareRect = new Rect[gridWidth][gridHeight];
        gridContent = new int[gridWidth][gridHeight];
        for(int x=0;x < gridWidth; x++)
        {
            for(int y=0;y < gridHeight; y++)
            {
                squareRect[x][y] = new Rect();
                squareRect[x][y].left = leftGridPadding + squareSize *x;
                squareRect[x][y].top = topGridPadding + squareSize *y;
                squareRect[x][y].right = squareRect[x][y].left + squareSize;
                squareRect[x][y].bottom = squareRect[x][y].top + squareSize;
            }
        }
        //startPointRect = new Rect();
        reSetGridMaze();
    }
    public void setShowImgOnly(boolean showImgOnly){
        this.showImgOnly = showImgOnly;
        postInvalidate();
    }
    public void setAutoUpdate(boolean autoUpdate)
    {
        this.autoUpdate = autoUpdate;
    }

    public void reSetGridMaze()
    {
        idBlocks.clear();
        for(int x=0;x < gridWidth; x++)
        {
            for(int y=0;y < gridHeight; y++)
            {
               gridContent[x][y] = GRID_UNEXPLORED;
            }
        }
    }
    public void reSetMazePref()
    {
        obstacleString = "";
        explorationString = "c000000000000000000000000000000000000000000000000000000000000000000000000003";
        PrefManager.write(OBSTACLE_STRING , obstacleString);
        PrefManager.write(EXPLORATION_STRING , explorationString);
        PrefManager.write(IMAGE_STRING ,"");
    }
    private void initBitmaps()
    {
        robotBitmap0 = BitmapFactory.decodeResource(getResources(), R.drawable.robot);
        robotBitmap0 = getResizedBitmap(robotBitmap0 , squareSize *robotWidth, squareSize *robotHeight);
        robotBitmap90 =rotateBitmap(robotBitmap0 , 90);
        robotBitmap180 =rotateBitmap(robotBitmap0 , 180);
        robotBitmap270 =rotateBitmap(robotBitmap0 , 270);
        idBlocksBitmap[0] = BitmapFactory.decodeResource(getResources(), R.drawable.id_block1_image_up_arrow);
        idBlocksBitmap[1] = BitmapFactory.decodeResource(getResources(), R.drawable.id_block2_image_down_arrow);
        idBlocksBitmap[2] = BitmapFactory.decodeResource(getResources(), R.drawable.id_block3_image_right_arrow);
        idBlocksBitmap[3] = BitmapFactory.decodeResource(getResources(), R.drawable.id_block4_image_left_arrow);
        idBlocksBitmap[4] = BitmapFactory.decodeResource(getResources(), R.drawable.id_block5_image_circle);
        idBlocksBitmap[5] = BitmapFactory.decodeResource(getResources(), R.drawable.id_block6_image_6);
        idBlocksBitmap[6] = BitmapFactory.decodeResource(getResources(), R.drawable.id_block7_image_7);
        idBlocksBitmap[7] = BitmapFactory.decodeResource(getResources(), R.drawable.id_block8_image_8);
        idBlocksBitmap[8] = BitmapFactory.decodeResource(getResources(), R.drawable.id_block9_image_9);
        idBlocksBitmap[9] = BitmapFactory.decodeResource(getResources(), R.drawable.id_block10_image_0);
        idBlocksBitmap[10] = BitmapFactory.decodeResource(getResources(), R.drawable.id_block11_image_v);
        idBlocksBitmap[11] = BitmapFactory.decodeResource(getResources(), R.drawable.id_block12_image_w);
        idBlocksBitmap[12] = BitmapFactory.decodeResource(getResources(), R.drawable.id_block13_image_x);
        idBlocksBitmap[13] = BitmapFactory.decodeResource(getResources(), R.drawable.id_block14_image_y);
        idBlocksBitmap[14] = BitmapFactory.decodeResource(getResources(), R.drawable.id_block15_image_z);
        for(int i= 0; i < idBlocksBitmap.length;i++)
        {
            idBlocksBitmap[i] = getResizedBitmap(idBlocksBitmap[i] , squareSize, squareSize);
        }
    }
    private void initPaint()
    {
//        startPointPaint = new Paint();
//        startPointPaint.setColor(Color.parseColor("#785cb85c"));
        strokePaint = new Paint();
        strokePaint.setStyle(Paint.Style.STROKE);
        strokePaint.setColor(Color.BLACK);
        strokePaint.setStrokeWidth(1);

        unexploredPaint = new Paint();
        unexploredPaint.setColor(Color.parseColor("#d9534f"));
        exploredPaint = new Paint();
        exploredPaint.setColor(Color.parseColor("#f7f7f7"));
        obstaclePaint = new Paint();
        obstaclePaint.setColor(Color.parseColor("#292b2c"));
        wayPointPaint = new Paint();
        wayPointPaint.setColor(Color.BLUE);
        wayPointPaint.setTextAlign(Paint.Align.CENTER);
        wayPointPaint.setTextSize(30);
        gridNumberPaint = new Paint();
        gridNumberPaint.setColor(Color.BLACK);
        gridNumberPaint.setTextSize(10);
        gridNumberPaint.setTextAlign(Paint.Align.CENTER);
    }
    private Paint getPaint(int paintIndex){
        switch(paintIndex){
            case GRID_UNEXPLORED:
                return unexploredPaint;
            case GRID_EXPLORED:
                return exploredPaint;
            case GRID_OBSTACLE:
                return obstaclePaint;
            default:
                return unexploredPaint;
        }
    }
    public GridMazeView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public GridMazeView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public GridMazeView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        for(int x=0;x < gridWidth; x++)
        {
            for(int y=0;y < gridHeight; y++)
            {
                canvas.drawRect(squareRect[x][y] , getPaint(gridContent[x][y]));
                canvas.drawRect(squareRect[x][y] , strokePaint);
            }
        }
        if(wayPointX != -1 && wayPointY != -1)
        {
            canvas.drawText( "w",squareRect[wayPointX][wayPointY].centerX() ,squareRect[wayPointX][wayPointY].centerY() + 10  , wayPointPaint);
        }
//        if(isRobotWithinBoundaries(startPointX,startPointY))
//        {
//            startPointRect.left = squareRect[startPointX][startPointY].left;
//            startPointRect.top = squareRect[startPointX][startPointY].top;
//            startPointRect.right = startPointRect.left + SQUARE_SIZE*robotWidth;
//            startPointRect.bottom = startPointRect.top + SQUARE_SIZE*robotHeight;
//            canvas.drawRect(startPointRect, startPointPaint);
//        }
        if(showImgOnly)
        {
            for( IdBlock idBlock : idBlocks)
            {
//            if(imgOnlyObs && gridContent[idBlock.getX()][yCoordInOtherOrigin(idBlock.getY())] != GRID_OBSTACLE)
//            {
//                continue;
//            }
                canvas.drawBitmap(idBlocksBitmap[idBlock.getId()],squareRect[idBlock.getX()][yCoordInOtherOrigin(idBlock.getY())].left , squareRect[idBlock.getX()][yCoordInOtherOrigin(idBlock.getY())].top , null);
            }
        }

        if(isRobotWithinBoundaries(robotPosX,robotPosY))
        {
            Log.d("debug" , "robot");
            canvas.drawBitmap(getRobotBitmap(),squareRect[robotPosX][robotPosY].left,squareRect[robotPosX][robotPosY].top , null);
        }
        drawGridNumbers(canvas);
    }

    private void drawGridNumbers(Canvas canvas) {
        for(int x=0; x< gridWidth; x++)
        {
            canvas.drawText(String.valueOf(x) , x* squareSize + leftGridPadding + squareSize/2 , gridHeight* squareSize + topGridPadding*2f, gridNumberPaint);
        }
        for(int y=0; y<gridHeight; y++)
        {
            canvas.drawText(String.valueOf(yCoordInOtherOrigin(y)) , leftGridPadding/2 , y* squareSize + topGridPadding + 3 + squareSize /2 , gridNumberPaint);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        boolean value = super.onTouchEvent(event);
//        if(autoUpdate)
//        {
//            return value;
//        }
        switch(event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
            {
                float x = event.getX();
                float y = event.getY();
                int col = convertXtoRowIndex(x);
                int row = convertYtoColIndex(y);
                if((!( col >= gridWidth || row >= gridHeight))&& currentContentToChange != -1)
                {
                    changeGridContent(x,y);
                    postInvalidate();
                }

                Log.d("debug" ,"x: " + convertXtoRowIndex(x) );
                Log.d("debug" ,"y: " + yCoordInOtherOrigin(convertYtoColIndex(y)) );
                return true;
            }
        }

        return value;
    }

    private int convertXtoRowIndex(float x ){
        x -=leftGridPadding;
        if(x < 10)
        {
            return 0;
        }
        else
        {
            return (int) ((x-10) / squareSize);
        }
    }

    private int convertYtoColIndex(float y ){
        if(y < 10)
        {
            return 0;
        }
        else
        {
            return (int) ((y-10) / squareSize);
        }
    }
    private void changeGridContent(float x ,float y){
        if(currentContentToChange != -1)
        {
            int col = convertXtoRowIndex(x);
            int row = convertYtoColIndex(y);
            if(currentContentToChange == GRID_WAYPOINT)
            {
                pressedWayPointX = col;
                pressedWaypointY = row;

                alertDialog.setTitle("Confirm Waypoint");
                alertDialog.setMessage("Set Waypoint to ("+col + "," + yCoordInOtherOrigin(row) +") ?");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                setWaypoint();

                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                            }
                        });
                alertDialog.show();

            }
            else if(currentContentToChange == GRID_STARTPOINT)
            {
                if(isStartPointWithinBoundaries(col, row))
                {
//                    startPointX = col;
//                    startPointY = row;
                    setRobotPos(col,row,0);
                    BluetoothConnectionService.write(("coordinates(" + col + ","+yCoordInOtherOrigin(row)+")").getBytes());
                }

            }
            else if(currentContentToChange == GRID_ROBOTPOS)
            {
                if(isRobotWithinBoundaries(col, row))
                {
                    setRobotPos(col,row,0);
                }

            }
            else {
                gridContent[col][row] = currentContentToChange;
            }
        }
    }
    public void setWaypoint(){
        wayPointX = pressedWayPointX;
        wayPointY = pressedWaypointY;
        BluetoothConnectionService.write(("p" + wayPointX + ","+yCoordInOtherOrigin(wayPointY)).getBytes());

        postInvalidate();
    }
    public void setCurrentContentToChange(int toChange){
            currentContentToChange = toChange;
    }
    public void disableCurrentContentToChange(int disable){
        if(currentContentToChange == disable){
            currentContentToChange = -1;
        }
    }
    public static float convertDpToPixel(float dp, Context context){
        return dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }
    public void getMDFfromPref()
    {
        obstacleString = PrefManager.read(OBSTACLE_STRING,"c000000000000000000000000000000000000000000000000000000000000000000000000003");
        explorationString = PrefManager.read(EXPLORATION_STRING,"");
        getImageFromPref();
        String[] roboPosArray = PrefManager.read(ROBOT_POSITION_STRING, "0,17,0").split(",");
        robotPosX = Integer.parseInt(roboPosArray[0]);
        robotPosY = Integer.parseInt(roboPosArray[1]);
        rotation = Integer.parseInt(roboPosArray[2]);
    }
    public void getImageFromPref(){
        String imageString = PrefManager.read(IMAGE_STRING, "");
        if(imageString.equals(""))
        {
            return;
        }
        String[] imageArray = imageString.split(",");
        for(int i = 0;i < imageArray.length;i+=3)
        {
            idBlocks.add(new IdBlock(Integer.parseInt(imageArray[i]) , Integer.parseInt(imageArray[i+1]) ,Integer.parseInt(imageArray[i+2])));
        }
    };

    public String getObstacleString() {
        return obstacleString;
    }

    public void setObstacleString(String obstacleString) {
        this.obstacleString = obstacleString;
        PrefManager.write(OBSTACLE_STRING , obstacleString);
    }

    public String getExplorationString() {
        return explorationString;
    }

    public void setExplorationString(String explorationString) {
        this.explorationString = explorationString;
        PrefManager.write(EXPLORATION_STRING , explorationString);
    }
    public void createMazeFromMdfAMD()
    {
        //rmbr there is 11 padded at both ends for bitExploration
        String bitExploration = hexToBin(explorationString);
        String bitObstacle = hexToBin(obstacleString);
        Log.d("debug" , "Explored length" + bitExploration.length());
        Log.d("debug" , "obstacle length" + bitObstacle.length());
        int obstacleIndex = bitObstacle.length()-1;
        for(int i = bitExploration.length()-3; i > 1; i--)
        {
            int col = (i-2)% gridWidth;
            int row = (i-2)/ gridWidth;

                if(bitExploration.charAt(i) == '1') //if explored
                {
                    if(bitObstacle.charAt(obstacleIndex) == '1') //if explored is obstacle
                    {
                        gridContent[col][row] = GRID_OBSTACLE;
                    }
                    else
                    {
                        gridContent[col][row] = GRID_EXPLORED;
                    }
                    obstacleIndex--;
                }
                else
                {
                    gridContent[col][row] = GRID_UNEXPLORED;
                }
        }
        postInvalidate();
    }
    public void createMazeFromMDF()
    {
        try {
            //  Block of code to try
            //rmbr there is 11 padded at both ends for bitExploration
            String bitExploration = hexToBin(explorationString);
            String bitObstacle = "";
            if(obstacleString.equals("") == false)
            {
                bitObstacle = hexToBin(obstacleString);
            }

            Log.d("debug" , "Explored length" + bitExploration.length());
            Log.d("debug" , "obstacle length" + bitObstacle.length());
            int obstacleIndex = 0;
            for(int i = 2; i <= bitExploration.length()-3; i++)
            {
                int col = (i-2)% gridWidth;
                int row = yCoordInOtherOrigin((i-2)/ gridWidth);

                if(bitExploration.charAt(i) == '1') //if explored
                {
                    Log.d("debug" , "explored cell: " + row);
                    if(bitObstacle.charAt(obstacleIndex) == '1') //if explored is obstacle
                    {
                        gridContent[col][row] = GRID_OBSTACLE;
                    }
                    else
                    {
                        gridContent[col][row] = GRID_EXPLORED;
                    }
                    obstacleIndex++;
                }
                else
                {
                    gridContent[col][row] = GRID_UNEXPLORED;
                }
            }
            postInvalidate();
        }
        catch(Exception e) {
            //  Block of code to handle errors
            Log.e("debug" ,"exception",  e);
            e.printStackTrace();
            Toast.makeText(getContext(),"MDF string error" , Toast.LENGTH_LONG).show();
        }

    }
    public void createMazeFromMDFlMsbPadding()
    {
        try {
            //  Block of code to try
            //rmbr there is 11 padded at both ends for bitExploration
            String bitExploration = hexToBin(explorationString);
            String bitObstacle = hexToBin(obstacleString);
            Log.d("debug" , "Explored length" + bitExploration.length());
            Log.d("debug" , "obstacle length" + bitObstacle.length());
            int obstacleIndex = bitObstacle.length()-1;
            for(int i = bitExploration.length()-3; i > 1; i--)
            {
                int col = (i-2)% gridWidth;
                int row = yCoordInOtherOrigin((i-2)/ gridWidth);

                if(bitExploration.charAt(i) == '1') //if explored
                {
                    Log.d("debug" , "explored cell: " + row);
                    if(bitObstacle.charAt(obstacleIndex) == '1') //if explored is obstacle
                    {
                        gridContent[col][row] = GRID_OBSTACLE;
                    }
                    else
                    {
                        gridContent[col][row] = GRID_EXPLORED;
                    }
                    obstacleIndex--;
                }
                else
                {
                    gridContent[col][row] = GRID_UNEXPLORED;
                }
            }
            postInvalidate();
        }
        catch(Exception e) {
            //  Block of code to handle errors
            Log.d("debug" , e.getMessage());
            Toast.makeText(getContext(),"MDF string error" , Toast.LENGTH_LONG).show();
        }

    }
    public static String hexToBin(String s) {
        int numBits = s.length() * 4;
        String value = new BigInteger(s, 16).toString(2);
        //padding of bits
        return String.format("%"+ numBits+ "s", value).replace(" ", "0");
    }
    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }
    public void setRobotPos(int x , int y , String rot)
    {
        Log.d("debug" , "String ROBOTPOS");
        robotPosX = x;
        robotPosY = y;
        switch(rot)
        {
            case "N":
                rotation = 0;
                break;
            case "E":
                rotation = 90;
                break;
            case "S":
                rotation = 180;
                break;
            case "W":
                rotation = 270;
                break;

        }
        PrefManager.write(ROBOT_POSITION_STRING, robotPosX+ "," + robotPosY + "," + rotation );
        postInvalidate();
    }
    public void setRobotPos(int x , int y , int rot)
    {
        Log.d("debug" , "int ROBOTPOS");
        robotPosX = x;
        robotPosY = y;
        rotation = rot;
        PrefManager.write(ROBOT_POSITION_STRING, robotPosX+ "," + robotPosY + "," + rotation );
        postInvalidate();
    }

    public Bitmap rotateBitmap(Bitmap original, float degrees) {
        int width = original.getWidth();
        int height = original.getHeight();

        Matrix matrix = new Matrix();
        matrix.preRotate(degrees);

        Bitmap rotatedBitmap = Bitmap.createBitmap(original, 0, 0, width, height, matrix, true);

        return rotatedBitmap;
    }

    private Bitmap getRobotBitmap()
    {
        switch(rotation)
        {
            case 0:
                return robotBitmap0;
            case 90:
                return robotBitmap90;
            case 180:
                return robotBitmap180;
            case 270:
                return robotBitmap270;
            default:
                return robotBitmap0;
        }
    }

    private boolean isRobotWithinBoundaries(int col , int row)
    {
        return !(row + robotHeight > gridHeight || col + robotWidth > gridWidth || row <0 || col < 0);
    }
    private boolean isStartPointWithinBoundaries(int col , int row)
    {
        return (row >= gridHeight/2);
    }
    // origin of map should start from bottom left and not top left.
    //use this to convert y coord based on correct origin
    private int yCoordInOtherOrigin(int y){
        return (gridHeight-1) - y;
    }

    public void addNumberIdBlocks(int id , int x , int y){
        if(id-1 >= 0 && id-1 <= numOfIdBlocks && x >=0 && x < gridWidth && y >=0 && y < gridHeight)
        {
            IdBlock newIdBlock = new IdBlock(x , y , id -1);
            //check if is on a obstacle
            if(gridContent[newIdBlock.getX()][yCoordInOtherOrigin(newIdBlock.getY())] != GRID_OBSTACLE)
            {
                return;
            }
            //check is exists already
            for( IdBlock idBlock : idBlocks)
            {
                if(idBlock.getId() == newIdBlock.getId())
                {
                    Toast.makeText(getContext(),"Same Image received" , Toast.LENGTH_LONG).show();
                    return;
                }
            }
            idBlocks.add(newIdBlock);
            if(idBlocksString != "")
            {
                idBlocksString =idBlocksString + ",";
            }
            idBlocksString = idBlocksString + x + ","+ y +"," + (id-1);
            PrefManager.write(IMAGE_STRING ,idBlocksString);
            postInvalidate();
        }
    }
    public String getImageString(){
        String temp = "{ ";
        boolean isFirst = true;
        for(int i = 0; i < idBlocks.size(); i ++)
        {
            IdBlock idBlock = idBlocks.get(i);
            if(isFirst == false)
            {
                temp = temp +",";
            }
            else
            {
                isFirst = false;
            }
            temp = temp + "(" + (idBlock.getId()+1) + "," + idBlock.getX()+ "," + idBlock.getY() +  ")";

        }
        temp = temp + " }";
        return temp;
    }

    public String getImageStringForImageTile(){
        String temp = "";
        boolean isFirst = true;
        for(int i = 0; i < idBlocks.size(); i ++)
        {
            IdBlock idBlock = idBlocks.get(i);
            if(isFirst == false)
            {
                temp = temp +",";
            }
            else
            {
                isFirst = false;
            }
            temp = temp + (idBlock.getId()+1) + "," + idBlock.getX()+ "," + idBlock.getY();

        }
        return temp;
    }

    private class IdBlock
    {
        private int x,y,id;
        public IdBlock(int x,int y,int id)
        {
            this.x = x;
            this.y = y;
            this.id = id;
        }
        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
